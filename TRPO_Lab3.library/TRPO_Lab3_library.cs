﻿using System;
namespace TRPO_Lab3_library
{
    public class Class1
    {
        public double Result (double P1, double P2, double H)
        {
            if (H <= 0 || P1 <= 0 || P2 <= 0)
                throw new ArgumentException("Одно или несколько из введенных значений равны или меньше нуля");
            return (((P1 + P2) / 2) * H);
        }
    }
}