﻿using System;
namespace TRPO_Lab3.consoleapp
{
    class Program
    {
        static void Main(string[] args)
        {
            double H, P1, P2;
            while(true)
            {
                Console.WriteLine("Введите значение высоты и двух площадей: ");
                Console.Write("H = ");
                H = Convert.ToDouble(Console.ReadLine());
                Console.Write("P1 = ");
                P1 = Convert.ToDouble(Console.ReadLine());
                Console.Write("P2 = ");
                P2 = Convert.ToDouble(Console.ReadLine());
                if ((H <= 0) || (P1 <= 0) || (P2 <= 0))
                {
                    Console.WriteLine("Одно или несколько значений равны или меньше нуля!");
                    continue;
                }
                else
                {
                    double Result1 = new TRPO_Lab3_library.Class1().Result(H, P1, P2);
                    Console.WriteLine(Result1);
                    break;
                }
            }
        }
    }
}    

