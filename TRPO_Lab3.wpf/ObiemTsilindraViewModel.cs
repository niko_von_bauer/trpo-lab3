﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TRPO_Lab3.wpf
{
    class ObiemTsilindraViewModel : INotifyPropertyChanged
    {
        private double P1;
        private double P2;
        private double H;

        public double Pl1
        {
            get { return P1; }

            set
            {
                P1 = value;
                OnPropertyChanged("Pl1");
                OnPropertyChanged("Result");
            }
        }
        public double Pl2
        {
            get { return P2; }

            set
            {
                P2 = value;
                OnPropertyChanged("Pl2");
                OnPropertyChanged("Result");
            }
        }

        public double He
        {
            get { return H; }

            set
            {
                H = value;
                OnPropertyChanged("He");
                OnPropertyChanged("Result");
            }
        }

        public double Result
        {
            get { return Math.Round(new TRPO_Lab3_library.Class1().Result(P1, P2, H), 2); }
        }

        public ObiemTsilindraViewModel()
        {
            Pl1 = 1;
            Pl2 = 1;
            He = 1;
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
