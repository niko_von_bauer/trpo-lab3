﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace TRPO_Lab3.wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ObiemTsilindraViewModel();
        }
    }
}
