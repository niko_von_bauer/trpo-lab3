using NUnit.Framework;
using System;

namespace TRPO_Lab3.test
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            const int P1 = 1;
            const int P2 = 1;
            const int H = 1;
            double result = new TRPO_Lab3_library.Class1().Result(P1, P2, H);
            double Itog = (((P1 + P2) / 2) * H);
            Assert.AreEqual(Itog, result);
        }
        [Test]
        public void Test2()
        {
            const int P1 = -1;
            const int P2 = -1;
            const int H = -1;
            Assert.Throws<ArgumentException>(() => new TRPO_Lab3_library.Class1().Result(P1,P2,H));
        }
    }
}